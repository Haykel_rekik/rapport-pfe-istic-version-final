\contentsline {chapter}{D\'edicaces}{i}{chapter*.1}
\contentsline {chapter}{Remerciements}{ii}{chapter*.2}
\contentsline {chapter}{Introduction G\'en\'erale}{1}{chapter*.6}
\contentsline {chapter}{\numberline {1}\'ETUDE PR\'EALABLE}{2}{chapter.1}
\contentsline {section}{Introduction}{2}{section*.7}
\contentsline {section}{\numberline {1.1}Pr\'esentation de l'organisme d'accueil}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Pr\'esentation du technopark elghazela }{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Pr\'esentation du cyberparc }{3}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Services et domaines d'activit\'e du cyberparc}{3}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Pr\'esentation du projet}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Contexte du projet}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Probl\'ematique}{4}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Objectifs du projet}{4}{subsection.1.2.3}
\contentsline {section}{Conclusion}{5}{section*.8}
\contentsline {chapter}{\numberline {2}Analyse des besoins}{6}{chapter.2}
\contentsline {section}{Introduction}{6}{section*.9}
\contentsline {section}{\numberline {2.1}Identification des acteurs}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Besoins fonctionnels et non fonctionnels}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Besoins fonctionnels}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Besoins non fonctionnels}{7}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Diagramme de cas d'utilisation}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Cas d'utilisation g\'en\'eral}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Description du cas d'utilisation "D\'eposer une demande de location"}{9}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Cas d'utilisation g\'erer les locations}{9}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Cas d'utilisation g\'erer contrat }{11}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Cas d'utilisation g\'erer soci\'et\'es }{12}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Description du cas d'utilisation "modifier soci\'et\'e"}{13}{subsection.2.3.6}
\contentsline {subsection}{\numberline {2.3.7}Cas d'utilisation g\'erer r\'eclamations et incidents }{13}{subsection.2.3.7}
\contentsline {subsection}{\numberline {2.3.8}Cas d'utilisation traiter une r\'eclamation ou un incident }{16}{subsection.2.3.8}
\contentsline {subsection}{\numberline {2.3.9}Cas d'utilisation g\'erer communication }{17}{subsection.2.3.9}
\contentsline {subsection}{\numberline {2.3.10}Cas d'utilisation g\'erer \'ev\'enements }{20}{subsection.2.3.10}
\contentsline {subsection}{\numberline {2.3.11}Description du cas d'utilisation "participer \`a un \'ev\'enement"}{22}{subsection.2.3.11}
\contentsline {subsection}{\numberline {2.3.12}Cas d'utilisation g\'erer cyberparc }{22}{subsection.2.3.12}
\contentsline {subsection}{\numberline {2.3.13}Cas d'utilisation gestion employ\'ee du cyberparc }{23}{subsection.2.3.13}
\contentsline {subsection}{\numberline {2.3.14}Cas d'utilisation gestion employ\'es des soci\'et\'es}{24}{subsection.2.3.14}
\contentsline {section}{Conclusion}{24}{section*.10}
\contentsline {chapter}{\numberline {3}\'ETUDE CONCEPTUELLE}{25}{chapter.3}
\contentsline {section}{Introduction}{25}{section*.11}
\contentsline {section}{\numberline {3.1}Architecture de l'application}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}Conception d\'etaill\'ee}{26}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Diagrammes de s\'equences}{26}{subsection.3.2.1}
\contentsline {subsubsection}{Diagramme de s\'equence "traiter demande de location"}{27}{section*.12}
\contentsline {subsubsection}{Diagramme de s\'equence "ajouter local"}{27}{section*.13}
\contentsline {subsubsection}{Diagramme de s\'equence "modifier local"}{28}{section*.14}
\contentsline {subsubsection}{Diagramme de s\'equence "supprimer local"}{29}{section*.15}
\contentsline {subsubsection}{Diagramme de s\'equence "afficher local"}{30}{section*.16}
\contentsline {subsubsection}{Diagramme de s\'equence "communiquer par messages"}{31}{section*.17}
\contentsline {subsection}{\numberline {3.2.2}Diagramme de classe}{31}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Probl\'ematique technique et solutions propos\'ees}{36}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Probl\'ematique technique}{36}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Solutions propos\'ees}{36}{subsection.3.3.2}
\contentsline {section}{Conclusion}{42}{section*.20}
\contentsline {chapter}{\numberline {4}R\'ealisation}{44}{chapter.4}
\contentsline {section}{Introduction}{44}{section*.21}
\contentsline {section}{\numberline {4.1}Environnement de travail}{44}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Environnement mat\'eriel}{44}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Environnement logiciel}{45}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Choix technologiques}{46}{section.4.2}
\contentsline {section}{\numberline {4.3}M\'ethodologie de travail}{47}{section.4.3}
\contentsline {section}{\numberline {4.4}Modules r\'ealis\'es}{47}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Partie de l'application destin\'ee aux administrateurs}{48}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Partie de l'application destin\'ee aux responsables du cyberparc}{52}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Partie de l'application destin\'ee aux utilisateurs}{54}{subsection.4.4.3}
\contentsline {section}{Introduction}{55}{section*.22}
\contentsline {chapter}{\numberline {5}Test}{56}{chapter.5}
\contentsline {section}{Introduction}{56}{section*.23}
\contentsline {section}{\numberline {5.1}Gestion des tests avec JUnit}{56}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Les m\'ethodes de JUnit}{56}{subsection.5.1.1}
\contentsline {section}{Conclusion}{57}{section*.24}
\contentsline {chapter}{Conclusion G\'en\'erale}{58}{chapter*.25}
\contentsline {chapter}{Annexe A}{59}{chapter*.26}
\contentsline {chapter}{Webographie}{64}{chapter*.32}
